# Movies REST API


# Sample usage of Movies Facade API

Finding movie (with details and comments) by id:

`curl -v -H 'Authorization: Basic dXNlcjpwYXNzd29yZA==' http://localhost:8080/movie/123`


Creating new movie as admin:

`curl -X POST -H 'Authorization: Basic YWRtaW46cGFzc3dvcmQ=' -H 'Content-Type: application/json' -d "{\"id\":\"300\", \"title\": \"My new title\", \"description\": \"Lorem ipsum\"}" -v http://localhost:8080/movie`


# Sample usage of backend services

Finding movie details by movie id:

`curl -v http://localhost:8081/movie/123/details`

Finding movie comments by movie id:

`curl -v http://localhost:8082/movie/123/comments`

