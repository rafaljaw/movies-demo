package com.example.movies.client;

import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.movies.model.Movie;

@FeignClient(name = "movie-details", url = "http://localhost:8081", decode404 = true)
public interface MovieDetailsClient {

  @Cacheable("movieDetails")
  @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/details")
  Optional<Movie> getMovie(@PathVariable("movieId") Integer movieId);
}
