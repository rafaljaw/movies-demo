package com.example.movies.client;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.movies.MovieCommentsCache;
import com.example.movies.model.Comment;

@Component
public class MovieCommentsClientFallback implements MovieCommentsClient {

  private final MovieCommentsCache movieCommentsCache;

  public MovieCommentsClientFallback(MovieCommentsCache movieCommentsCache) {
    this.movieCommentsCache = movieCommentsCache;
  }

  @Override
  public List<Comment> getComments(Integer movieId) {
    return movieCommentsCache.getComments(movieId);
  }
}
