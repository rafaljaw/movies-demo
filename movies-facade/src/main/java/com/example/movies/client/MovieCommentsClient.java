package com.example.movies.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.movies.model.Comment;

@FeignClient(name = "movie-comments", url = "http://localhost:8082", fallback = MovieCommentsClientFallback.class)
public interface MovieCommentsClient {

  @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/comments")
  List<Comment> getComments(@PathVariable("movieId") Integer movieId);
}
