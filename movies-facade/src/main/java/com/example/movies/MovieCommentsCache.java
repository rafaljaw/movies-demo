package com.example.movies;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.example.movies.model.Comment;

@Component
public class MovieCommentsCache {

  private final Map<Integer, List<Comment>> commentsByMovieId = new ConcurrentHashMap<>();

  public void put(Integer movieId, List<Comment> comments) {
    commentsByMovieId.put(movieId, comments);
  }

  public List<Comment> getComments(Integer movieId) {
    return commentsByMovieId.get(movieId);
  }
}
