package com.example.movies;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.annotation.security.RolesAllowed;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.client.MovieCommentsClient;
import com.example.movies.client.MovieDetailsClient;
import com.example.movies.exception.MovieNotFound;
import com.example.movies.model.Comment;
import com.example.movies.model.Movie;

@RestController
public class MoviesFacadeController {

  private final MovieDetailsClient movieDetailsClient;

  private final MovieCommentsClient movieCommentsClient;

  private final MovieCommentsCache movieCommentsCache;

  public MoviesFacadeController(MovieDetailsClient movieDetailsClient, MovieCommentsClient movieCommentsClient,
      MovieCommentsCache movieCommentsCache) {
    this.movieDetailsClient = movieDetailsClient;
    this.movieCommentsClient = movieCommentsClient;
    this.movieCommentsCache = movieCommentsCache;
  }

  @GetMapping("/movie/{movieId}")
  public Movie findById(@PathVariable Integer movieId) throws Throwable {
    CompletableFuture<Movie> movieSupplier = CompletableFuture.supplyAsync(
        () -> movieDetailsClient.getMovie(movieId).orElseThrow(() -> new MovieNotFound(movieId)));

    CompletableFuture<List<Comment>> commentsSupplier = CompletableFuture.supplyAsync(
        () -> movieCommentsClient.getComments(movieId));

    try {
      Movie movie = movieSupplier.get();
      List<Comment> comments = commentsSupplier.get();
      if (comments != null) {
        movieCommentsCache.put(movieId, comments);
      }
      movie.setComments(comments);
      return movie;
    } catch (ExecutionException e) {
      throw e.getCause();
    }
  }

  @RolesAllowed("ROLE_ADMIN")
  @PostMapping("/movie")
  @ResponseStatus(HttpStatus.CREATED)
  public void addMovie(@RequestBody Movie movie) {
    System.out.println("About to add movie " + movie.getTitle());
    //TODO create new movie...
  }

  @RolesAllowed("ROLE_USER")
  @PostMapping("/comment")
  @ResponseStatus(HttpStatus.CREATED)
  public void addComment(@RequestBody Comment comment) {
    System.out.println("About to add comment " + comment.getMessage());
    //TODO create new comment...
  }
}
