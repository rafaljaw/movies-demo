package com.example.movies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import com.example.movies.config.WebSecurityConfig;

@Import(WebSecurityConfig.class)
@SpringBootApplication
@EnableFeignClients
@EnableCaching
public class MoviesFacadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviesFacadeApplication.class, args);
	}
}
