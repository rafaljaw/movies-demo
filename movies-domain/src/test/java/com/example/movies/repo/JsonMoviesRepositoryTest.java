package com.example.movies.repo;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.example.movies.impl.JsonMoviesRepository;
import com.example.movies.model.Movie;

public class JsonMoviesRepositoryTest {

  private JsonMoviesRepository jsonMoviesRepository = new JsonMoviesRepository();

  @Test
  public void shouldReadSampleData() {
    // when
    Movie movie = jsonMoviesRepository.findById(1).orElseThrow(() -> new IllegalArgumentException("Unable to find movie with id 1"));

    // then
    assertNotNull(movie);
  }
}