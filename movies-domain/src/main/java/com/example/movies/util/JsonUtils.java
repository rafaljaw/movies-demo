package com.example.movies.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static <T> Collection<T> loadItems(Class<T> clazz, String resource) {
    try {
      InputStream resourceAsStream = JsonUtils.class.getClassLoader().getResourceAsStream(resource);
      String itemsAsJson = StreamUtils.copyToString(resourceAsStream, StandardCharsets.UTF_8);

      Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + clazz.getName() + ";");

      return Arrays.asList(objectMapper.readValue(itemsAsJson, arrayClass));
    } catch (IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
