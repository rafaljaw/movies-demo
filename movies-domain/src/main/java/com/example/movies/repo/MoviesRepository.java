package com.example.movies.repo;

import java.util.Optional;

import com.example.movies.model.Movie;

public interface MoviesRepository {

  Optional<Movie> findById(Integer id);
}
