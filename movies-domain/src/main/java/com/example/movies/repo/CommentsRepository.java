package com.example.movies.repo;

import java.util.Collection;

import com.example.movies.model.Comment;

public interface CommentsRepository {

  Collection<Comment> findByMovieId(Integer movieId);
}
