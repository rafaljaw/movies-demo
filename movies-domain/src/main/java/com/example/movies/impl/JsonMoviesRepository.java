package com.example.movies.impl;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.movies.model.Movie;
import com.example.movies.repo.MoviesRepository;
import com.example.movies.util.JsonUtils;

@Component
public class JsonMoviesRepository implements MoviesRepository {

  private final Collection<Movie> movies;
  private final Map<Integer, Movie> moviesById;

  private static Collection<Movie> loadMoviewFromSampleData() {
    return JsonUtils.loadItems(Movie.class, "testdata/movies.json");
  }

  public JsonMoviesRepository() {
    this(loadMoviewFromSampleData());
  }

  public JsonMoviesRepository(Collection<Movie> movies) {
    this.movies = movies;
    this.moviesById = movies.stream().collect(Collectors.toMap(Movie::getId, Function.identity()));
  }

  @Override
  public Optional<Movie> findById(Integer id) {
    return Optional.ofNullable(moviesById.get(id));
  }
}
