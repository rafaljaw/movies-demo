package com.example.movies.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.movies.model.Comment;
import com.example.movies.repo.CommentsRepository;
import com.example.movies.util.JsonUtils;

@Component
public class JsonCommentsRepository implements CommentsRepository {

  private final Collection<Comment> comments;

  private final Map<Integer, List<Comment>> commentsByMovieId;

  public JsonCommentsRepository() {
    this(JsonUtils.loadItems(Comment.class, "testdata/comments.json"));
  }

  public JsonCommentsRepository(Collection<Comment> comments) {
    this.comments = comments;
    this.commentsByMovieId = comments.stream().collect(Collectors.groupingBy(Comment::getMovieId));
  }

  @Override
  public Collection<Comment> findByMovieId(Integer movieId) {
    return commentsByMovieId.getOrDefault(movieId, Collections.emptyList());
  }
}
