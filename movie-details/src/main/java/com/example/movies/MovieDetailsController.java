package com.example.movies;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.exception.MovieNotFound;
import com.example.movies.model.Movie;
import com.example.movies.repo.MoviesRepository;

@RestController
public class MovieDetailsController {

  private final MoviesRepository moviesRepository;

  public MovieDetailsController(MoviesRepository moviesRepository) {
    this.moviesRepository = moviesRepository;
  }

  @GetMapping("/movie/{id}/details")
  public Movie findById(@PathVariable Integer id) {
    Optional<Movie> movie = moviesRepository.findById(id);
    if (!movie.isPresent()) {
      throw new MovieNotFound(id);
    }
    return movie.get();
  }
}
