package com.example.movies;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.movies.model.Movie;
import com.example.movies.repo.MoviesRepository;

@RunWith(MockitoJUnitRunner.class)
public class MovieDetailsControllerTest {

  private static final Integer MOVIE_ID = 123;

  @Mock
  private MoviesRepository moviesRepository;

  @InjectMocks
  private MovieDetailsController movieDetailsController;

  @Test
  public void shouldDelegateMovieLookupToMoviesRepository() {
    // given
    Movie expectedMovie = new Movie();
    when(moviesRepository.findById(MOVIE_ID)).thenReturn(Optional.of(expectedMovie));

    // when
    Movie actualMovie = movieDetailsController.findById(MOVIE_ID);

    // then
    assertSame(expectedMovie, actualMovie);
    verify(moviesRepository).findById(MOVIE_ID);
  }
}
