package com.example.movies;

import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.movies.model.Comment;
import com.example.movies.repo.CommentsRepository;

@RestController
public class MovieCommentsController {

  private final CommentsRepository commentsRepository;

  public MovieCommentsController(CommentsRepository commentsRepository) {
    this.commentsRepository = commentsRepository;
  }

  @GetMapping("/movie/{movieId}/comments")
  public Collection<Comment> findById(@PathVariable Integer movieId) {
    return commentsRepository.findByMovieId(movieId);
  }
}
