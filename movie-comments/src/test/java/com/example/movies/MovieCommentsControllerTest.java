package com.example.movies;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.movies.model.Comment;
import com.example.movies.repo.CommentsRepository;

@RunWith(MockitoJUnitRunner.class)
public class MovieCommentsControllerTest {

  private static final Integer MOVIE_ID = 123;

  @Mock
  private CommentsRepository commentsRepository;

  @InjectMocks
  private MovieCommentsController movieCommentsController;

  @Test
  public void shouldDelegateCommentsLookupToCommentsRepository() {
    // given
    Collection<Comment> expectedComments = new ArrayList<>();
    when(commentsRepository.findByMovieId(MOVIE_ID)).thenReturn(expectedComments);

    // when
    Collection<Comment> actualComments = movieCommentsController.findById(MOVIE_ID);

    // then
    assertSame(expectedComments, actualComments);
    verify(commentsRepository).findByMovieId(MOVIE_ID);
  }
}